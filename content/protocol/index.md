---
title: Corona protocol
type: protocol
---



We beseffen ons dat iedereen door de Coronamaatregelen afgelopen maanden heel anders heeft doorgebracht. Sommige mensen waren veel drukker dan normaal, terwijl anderen veel meer alleen waren dan normaal. Op het KloosterBoerderijFestival Back to the roots willen we een ruimte creëren voor iedereen. Om tot rust te komen, om te ontmoeten. Om dat wat je de afgelopen maanden hebt ontdekt een plek te geven en om alle frustratie die er over maatregelen is er even uit te gooien. Voor sommige mensen is het misschien spannend om zich weer onder mensen te geven, ‘out of your corona comfort zone’, terwijl anderen superblij zijn dat ze eindelijk weer in een groep kunnen zijn. Wat we belangrijk hierin vinden is dat er ruimte is voor iedereen.

### Vooraf
Je ontvangt hierbij informatie over hoe we met de maatregelen van RIVM omtrent COVID-19 omgaan.
  
- Er is een maximaal aantal plekken en alleen mensen die een hele week (of evt tot dinsdag) komen zijn welkom. Helaas is het te ingewikkeld om ook dagjesmensen te verwelkomen.
- Alleen als je je hebt geregistreerd en een bevestiging dat je welkom bent hebt gekregen, kun je deelnemen.
- Deelnemers worden gevraagd of zij COVID-19 gerelateerde klachten (zoals beschreven door het RIVM) hebben, of bij iemand in huis wonen met deze klachten. We vragen van iedereen bij aankomst een verklaring omtrent gezondheid te ondertekenen. 
- Heb je klachten? Kom dan niet en geef het door. 


### Wat wij doen

- We zorgen dat er genoeg ruimte is zodat de 1,5 meter afstand mogelijk is. We denken goed na over met name maaltijden en diensten, zodat deze veilig plaats kunnen vinden.
- Hygiëne: we zorgen dat er voldoende mogelijkheden zijn om je handen te wassen/ontsmetten. Samen zullen we sanitair en contactpunten vaak schoonmaken & ontsmetten. 
- Aan het begin van de week bespreken we met alle aanwezigen hoe we met de regels omtrent Covid-19 om willen gaan.

### Eigen verantwoordelijkheid

- Iedereen die komt heeft eigen verantwoordelijkheid om zich zo veel mogelijk in te zetten om besmetting te voorkomen.
- Geef elkaar de ruimte.
- Houd je aan de algemene hygiëne maatregelen (RIVM; handen wassen, in elleboog hoesten) en houd genoeg afstand.
- Zie je dat het ergens druk is, zoek dan een andere plek. 
- Heb geduld en wees lief voor elkaar.
- Ga met respect met elkaar om en met ieder zijn visie en denkwijze. Handel verstandig. Zo zorgen we samen dat de week leuk is voor iedereen.
- Zorg dat als jij of iemand in je nabije omgeving ziek is (tot 2 weken na het KBF) je dat aan de organisatie doorgeeft. 

Het organisatieteam doet haar best om het voor mensen mogelijk te maken om zich aan de richtlijnen van het RIVM te houden. De verantwoordelijkheid voor de eigen gezondheid ligt bij de bezoekers zelf. We zorgen dat de regels duidelijk zijn. Het organisatieteam spreekt mensen die zich duidelijk niet aan de regels houden aan, zeker als dit voor anderen overlast geeft. 
